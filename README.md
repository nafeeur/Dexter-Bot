# Dexter-Bot for Discord
![](https://i.imgur.com/GiUNrta.jpg)

**Dexter-Bot is a general purpose computational and query bot built with Python for Discord. The bot uses WolframAlpha API to solve wide range of computational problems in addition to provide answers to various trivia questions within Discord chat-box. As an additional feature, the bot is also equipped with Wikipedia API that provides user with a ~2000 word Wikipedia article summary of any requested topic.**

>Dexter-bot has two commands: 
- $db$ "command" &
- $dbwiki$ "command"

## License

    Dexter-Bot is a discord bot for computation and general inquiry.
    Copyright (C) 2021  Nafeeur Rahman

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
